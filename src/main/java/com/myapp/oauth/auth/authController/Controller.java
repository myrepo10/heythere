package com.myapp.oauth.auth.authController;

import com.myapp.oauth.auth.repository.DataEntity;
import com.myapp.oauth.auth.service.Authservice;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@RestController
public class Controller {

    //auth server has capabilities of registering user,
    // validating token, getting all users,  authenticating user, redirecting user

    @Autowired
    private Authservice auth;

    @PostMapping(path = "/add")
    public @ResponseBody
    ResponseEntity<TokenResponseDto> addNewUser(@RequestParam String username, @RequestParam String email, @RequestParam String password) {
        return ResponseEntity.ok(auth.userRegister(username,email, password));
    }

    @GetMapping("/validate-token")
    public String isValidToken(@RequestParam String token) {
        return auth.validateToken(token);
    }

    @GetMapping("/register")
    public ModelAndView getForm() {
        return new ModelAndView("formClient");
    }

    @GetMapping(path = "/all")
    public @ResponseBody Iterable <DataEntity> getAllUsers() {
        return auth.getAll();
    }

    @GetMapping("/logUser")
    public ModelAndView loginForm(){
        return new ModelAndView("loginForm");
    }

    @PostMapping(path = "/log")
    public @ResponseBody
    ResponseEntity<String> loginUser(@RequestParam String username, @RequestParam String email, @RequestParam String password, HttpServletRequest request, HttpServletResponse httpResponse) {
        String td = auth.userLoginHandle(username, email, password);
        if (td == null){
            return ResponseEntity.status(HttpStatus.FORBIDDEN).body(null);
        }
        else {
            try {
                httpResponse.sendRedirect(td);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return ResponseEntity.ok("Success");
        }
    }

}
