DROP TABLE IF EXISTS userslist;
CREATE TABLE userslist(id INT PRIMARY KEY AUTO_INCREMENT, username VARCHAR(100), password VARCHAR(100), email VARCHAR(100) UNIQUE, token VARCHAR(100));

INSERT INTO userslist(username, password, email) VALUES('greatguy', "youcantguessme", "great@gluglu.com");
INSERT INTO userslist(username, password, email) VALUES('badguy', "iamfast", "amazing@gluglu.com");
